import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';
import { NowplayingResponse, Movie } from '../interfaces/nowplaying-response';

import { MovieDetails } from '../interfaces/movie-details';
import { CreditsResponse, Cast } from '../interfaces/credits.response';

@Injectable({
  providedIn: 'root',
})
export class PeliculasService {
  private baseUrl = 'https://api.themoviedb.org/3';
  private carteleraPage = 1;
  constructor(private http: HttpClient) {}

  get params(): any {
    return {
      api_key: '251187fa3952fe84f2cc82a2a7afb2d6',
      language: 'es-ES',
      page: this.carteleraPage.toString(),
    };
  }

  resetCartelera(): void {
    this.carteleraPage = 1;
  }

  getCartelera(): Observable<NowplayingResponse> {
    return this.http
      .get<NowplayingResponse>(`${this.baseUrl}/movie/now_playing`, {
        params: this.params,
      })
      .pipe(
        tap(() => {
          this.carteleraPage += 1;
        })
      );
  }

  searchFilms(text: string): Observable<Movie[]> {
    const params = { ...this.params, page: '1', query: text };
    return this.http
      .get<NowplayingResponse>(`${this.baseUrl}/search/movie`, {
        params,
      })
      .pipe(map((resp) => resp.results));
  }

  getMovieDetail(id: string): Observable<MovieDetails> {
    return this.http
      .get<MovieDetails>(`${this.baseUrl}/movie/${id}`, {
        params: this.params,
      })
      .pipe(catchError((err) => of(null)));
  }

  getCast(id: string): Observable<Cast[]> {
    return this.http
      .get<CreditsResponse>(`${this.baseUrl}/movie/${id}/credits`, {
        params: this.params,
      })
      .pipe(
        map((resp) => resp.cast),
        catchError((err) => of([]))
      );
  }
}
