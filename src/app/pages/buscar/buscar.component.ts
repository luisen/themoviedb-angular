import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PeliculasService } from '../../services/peliculas.service';
import { Movie } from '../../interfaces/nowplaying-response';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css'],
})
export class BuscarComponent implements OnInit, OnDestroy {
  public text = '';
  public movies: Movie[] = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private peliculasService: PeliculasService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.text = params.text;

      this.peliculasService.searchFilms(params.text).subscribe((movies) => {
        this.movies = movies;
      });
    });
  }

  ngOnDestroy(): void {
    this.peliculasService.resetCartelera();
  }
}
